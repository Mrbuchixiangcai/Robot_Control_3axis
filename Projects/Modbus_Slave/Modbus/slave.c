/**
******************************************************************************
* 文件名程: slave.c 
* 作    者: 东莞-邓凯哥  微信D13631760681
* 功    能: Modbus从站接收主站数据并响应
* 硬    件: STM32F407 三轴伺服控制板
* 说明：此处程序参考了深圳显控公司触摸屏与51单片机通信范例 
* 所有权归深圳市显控自动化技术有限公司
* 软件版本 	
******************************************************************************
**/

#include "slave.h"

#define SALVEID 0x01

extern UART_HandleTypeDef huart1;

uint8_t  Bit[400];    		//bit[0]----[7]   存放X1-----X8  ////bit[8]-----[375]  类似于PLC  M1---M376继电器元件  //bit[383]---[391]  存放Y1---Y8
int16_t  Dword[320];  		//D1-----D320 
int16_t  *pd=Dword;
uint8_t  *pb=Bit;

uint8_t  SendBuf[128];        	//定义发送数组，最大允许发送128
uint8_t	 ReceBuf[128];	 		//定义接收数组，最大允许接收128
uint8_t	 RxCounter;				//串口接收计数
uint8_t  *ps=SendBuf;
uint8_t  *pr=ReceBuf;

/*****************************************************************************
**函数信息 ：CRC校验高位数据表                 
**功能描述 ：
**输入参数 ：
**输出参数 ：
*****************************************************************************/
uint8_t auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40
} ;

/*****************************************************************************
**函数信息 ：CRC校验低位数据表                 
**功能描述 ：
**输入参数 ：
**输出参数 ：
*****************************************************************************/
uint8_t auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
0x40
} ;

/*****************************************************************************
**函数信息 ：uint16_t Calc_CRC16(uint8_t *puchMsg,uint8_t usDataLen)                 
**功能描述 ：Modbus校验码计算
**输入参数 ：puchMsg 数据指针, usDataLen 数据长度
**输出参数 ：数据校验值
*****************************************************************************/
uint16_t Calc_CRC16(uint8_t *puchMsg,uint8_t usDataLen) 	
{ 											
	uint8_t uchCRCHi = 0xFF ; 				
	uint8_t uchCRCLo = 0xFF ; 				
	uint8_t uIndex ; 							
	while (usDataLen--) 					
	{
		uIndex = uchCRCHi ^ *puchMsg++ ; 	
		uchCRCHi = uchCRCLo ^ auchCRCHi[uIndex] ;
		uchCRCLo = auchCRCLo[uIndex] ;
	}
	return (((uint16_t)(uchCRCLo) << 8) | uchCRCHi) ;
}


/*****************************************************************************
**函数信息 ：Recirve_01(void)                
**功能描述 ：modbus 01功能码  读取X,Y,M寄存器数值
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Recirve_01()
 {
	 uint16_t Addr,Num,crc_end,sendlength,i,j;
	 
				Addr  =(*(pr+2)<<8)+*(pr+3);    		//要读取的起始地址
				Num   =(*(pr+4)<<8)+*(pr+5);				//要读取的字节数量,单位是位
				if((Addr+Num)<=0x190)              	//检测地址是否超出范围
				{
					/*STM32F103回传数据至威纶触摸屏*/
					*(ps+0)=SALVEID;         	//站号
					*(ps+1)=0x01;           	//功能码
					if((Num%8)==0)
					*(ps+2)=(Num)/8;      		//要返回的字节数
					else
					*(ps+2)=((Num)/8)+1;  		//不能整除8的时候要多返回一个字节
					for(i=0;i<*(ps+2);i++)				  
							{
							*(ps+3+i)=0;          //先清零复位
							for(j=0;j<8;j++)      //每8个位状态组成一个字节返回
									{
										*(ps+3+i)=((*(pb+Addr+i*8+j)&0x01)<<j)+*(ps+3+i);     //低位在前，高位在后
									}	                       
							}	
					crc_end=Calc_CRC16(ps,(*(ps+2)+3));					//校验
					*(ps+3+*(ps+2))=crc_end%256;											//校验低位
					*(ps+4+*(ps+2))=crc_end/256;											//校验高位
					sendlength=*(ps+2)+5; 														//数据长度
					HAL_UART_Transmit_IT(&huart1, ps,sendlength);							//开启串口3传输数据		
				}
	   }

/*****************************************************************************
**函数信息 ：Recirve_03(void)                
**功能描述 ：modbus 03功能码  读单个字寄存器的数值
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Recirve_03()
 {
			uint16_t Addr,Num,crc_end,sendlength,i,j;
			
				Addr  =(*(pr+2)<<8)+*(pr+3);    	//要读取的起始地址
				Num   =(*(pr+4)<<8)+*(pr+5);			//要读取的字节数量
				 if((Addr+Num)<=320) 							//检测地址是否超出范围
				 {
						/*STM32F103回传数据至威纶触摸屏*/
						*(ps+0)=SALVEID;            	//站号
						*(ps+1)=0x03;           			//功能码
						*(ps+2)=Num*2;      					//要返回的字节数是请求报文的第五个字节*2
						for(i=0;i<Num;i++)
					  {
							*(ps+3+i*2)=((*(pd+Addr+i))>>8)&0xff;   //返回寄存器值的高字节
							*(ps+4+i*2)=(*(pd+Addr+i))&0xff;        //返回寄存器值得低字节
					  }
						crc_end=Calc_CRC16(ps,(Num*2)+3);		//校验
						*(ps+3+Num*2)=crc_end%256;								//校验低位
						*(ps+4+Num*2)=crc_end/256;								//校验高位
						sendlength=(Num*2)+5; 										//数据长度										
						HAL_UART_Transmit_IT(&huart1, ps,sendlength);			//开启串口3传输数据
				 }
 }
 
/*****************************************************************************
**函数信息 ：Recirve_05(void)                
**功能描述 ：modbus 05功能码  写单个位线圈Y.....M....
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Recirve_05()
{
			uint16_t Addr,Writedata,crc_end,sendlength,i,j;
			
					Addr=(*(pr+2)<<8)+*(pr+3);    			//要写入的地址
					Writedata=(*(pr+4)<<8)+*(pr+5);    	//要写入的数据
					if((Addr>7)&(Addr<0x190))        		//写M,Y线圈       
					{
						if(Writedata==0xff00)   		//置位线圈
						*(pb+Addr)=1;
						if(Writedata==0x0000)   		//复位线圈				
						*(pb+Addr)=0;
						/*STM32F103回传数据至威纶触摸屏*/
						*(ps+0)=SALVEID;            //站号
						*(ps+1)=0x05;           		//功能码
						*(ps+2)=Addr>>8;     				//地址高字节
						*(ps+3)=Addr&0xff;     			//地址低字节
						*(ps+4)=Writedata>>8;     	//地址高字节
						*(ps+5)=Writedata&0xff;     //地址低字节
						crc_end=Calc_CRC16(ps,6);	//校验
						*(ps+6)=crc_end%256;					  //校验低位
						*(ps+7)=crc_end/256;					  //校验高位		
						sendlength=8; 									//数据长度
						HAL_UART_Transmit_IT(&huart1, ps,sendlength);//开启串口3传输数据
					}
} 

/*****************************************************************************
**函数信息 ：Recirve_06(void)                
**功能描述 ：modbus 06功能码  写数值到单个字寄存器
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Recirve_06()
{
	 
	 uint16_t Addr,Writedata,crc_end,sendlength,i,j;
	 
				Addr=(*(pr+2)<<8)+*(pr+3);    			//要写入的地址
				Writedata=(*(pr+4)<<8)+*(pr+5);			//要写入的数据
				if(Addr<=320)												//检测地址是否超出范围
					{
						*(pd+Addr)=Writedata;           //将数值写入寄存器
						/*STM32F103回传数据至威纶触摸屏*/
						*(ps+0)=SALVEID;            		//站号
						*(ps+1)=0x06;           				//功能码
						*(ps+2)=Addr>>8;      					//返回地址高字节
						*(ps+3)=Addr&0xff;      				//返回地址低字节
						*(ps+4)=(uint8_t)(((*(pd+Addr))>>8)&0xff);  //返回寄存器值高字节
						*(ps+5)=(uint8_t)(*(pd+Addr)&0xff);         //返回寄存器值低字节
						crc_end=Calc_CRC16(ps,6);	//校验
						*(ps+6)=crc_end%256;						//校验低位
						*(ps+7)=crc_end/256;						//校验高位
						sendlength=8; 									//数据长度
						HAL_UART_Transmit_IT(&huart1, ps,sendlength);//开启串口1传输数据
					}
					
}
 
/*****************************************************************************
**函数信息 ：Recirve_10(void)                
**功能描述 ：modbus 10功能码  写数值到多个字寄存器
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Recirve_10()
{
		uint16_t Addr,Num,crc_end,sendlength,i,length;
		
		Addr=(*(pr+2)<<8)+*(pr+3);    			//要写入的起始地址
		Num	=(*(pr+4)<<8)+*(pr+5);				//寄存器数量
		length=*(pr+6);							//要写的字节数量

		if((Addr+(length/2))<=320)          	//检测地址是否超出范围
			{
				for(i=0;i<(length/2);i++)       //将值写入寄存器
					 {
						 *(pd+Addr+i)=(*(pr+7+i*2)<<8)+(*(pr+8+i*2)&0xff);	
					 }
					 /*STM32F103回传数据至威纶触摸屏*/
					*(ps+0)=SALVEID;            					//站号
					*(ps+1)=0x10;           						//功能码
					*(ps+2)=Addr>>8;      							//返回地址高位
					*(ps+3)=Addr&0xff;      						//返回地址低位
					*(ps+4)=Num>>8;									//寄存器数量高位
					*(ps+5)=Num&0xff;								//寄存器数量低位
					crc_end=Calc_CRC16(ps,6);						//校验
					*(ps+6)=crc_end%256;							//校验低位
					*(ps+7)=crc_end/256;							//校验高位
					sendlength=8; 									//数据长度
					HAL_UART_Transmit_IT(&huart1, ps,sendlength);	//开启串口1传输数据
			}
} 

/*****************************************************************************
**函数信息 ：Modbus_Command(void)                
**功能描述 ：modbus 功能码解析并生成响应报文
**输入参数 ：无
**输出参数 ：无
*****************************************************************************/
void Modbus_Command()
{
		uint16_t crc_end;
		if(ReceBuf[0]==SALVEID)								//从站地址判断
	 {
			crc_end=Calc_CRC16(ReceBuf,RxCounter-2);					  		//校验
			if(crc_end==((ReceBuf[RxCounter-1]<<8 )|ReceBuf[RxCounter-2]))	  	//当校验一致时
			{
				switch(ReceBuf[1])							//功能码
				{
				case 0x01:Recirve_01(); break; 
				case 0x03:Recirve_03(); break; 
				case 0x05:Recirve_05(); break; 
				case 0x06:Recirve_06(); break; 
				case 0x10:Recirve_10(); break; 
				default:;break;  
				}
			}
	}
}