#ifndef __SLAVE_H
#define __SLAVE_H	 
#include "stm32f4xx_hal.h"


extern uint8_t  RxCounter,ReceBuf[128];	 
extern uint8_t  Bit[400];    //bit[0]----[7]   存放X1-----X8  ////bit[8]-----[375]  类似于PLC  M0---M376继电器元件  //bit[383]---[391]  存放Y1---Y8
extern int16_t  Dword[320];   //D1-----D320 

void Modbus_Command(void);	 	
#endif