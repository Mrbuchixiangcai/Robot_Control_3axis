/**
******************************************************************************
* 文件名程: speed.c 
* 作    者: 东莞-邓凯哥   微信:D13631760681
* 功    能: 伺服梯形加减速绝对位置定位
* 硬    件: 
* 说明：实现STM32输出T形加减速	PWM脉冲
* 软件版本 	1.01
******************************************************************************
**/
#include "speed.h"
#include "arm_math.h"

extern TIM_HandleTypeDef htim3;


Tspd SPD;

uint16_t acc[8000],dec[8000];   //这两个数组用来存放计算得到的加减速TIM3->ARR的更新值,以空间换时间
/*****************************************************************************
**函数信息 ：void TIM3_MoveAbs(uint32_t MaxFREQ,uint16_t AccTime,uint32_t Target)        
**功能描述 ：X轴绝对加减速运动计算
**输入参数 ：MaxFREQ 脉冲输出最高频率, AccTime 加减速时间MS, Target 目标位置
**输出参数 ：无
**说明:
*****************************************************************************/
void TIM3_MoveAbs(uint32_t MaxFREQ,uint16_t AccTime,uint32_t Target)
{
	float acel,tac;
	uint32_t accnt,step;
	uint16_t lad,i;
	
	if(MaxFREQ<Min_Freq|MaxFREQ>100000)		//目标频率小于5000或大于100000不执行加减速定位
			return;
	
	acel=(float)((MaxFREQ-Min_Freq)/AccTime*1000.0);
	accnt=(MaxFREQ+Min_Freq)/2*AccTime/1000;
	SPD.count=0;
	SPD.maxfreq=MaxFREQ;
	SPD.minfreq=Min_Freq;
	SPD.accel=(uint32_t)acel;
	if((SPD.current-Target)!=0)   		//判断当前位置与目标位置是否一致
	{
		if(SPD.current<Target)			//当前位置小于目标位置,电机正转
		{
			SPD.dir=CW;					//脉冲正计数
			step=Target-SPD.current;  	//计算要移动的脉冲数量
				if(step<20)
				{
					SPD.run_state=DECEL;		//直接做减速
					SPD.decel_val=step;			//减速步
					for(i=0;i<SPD.decel_val+1;i++)
					{
						dec[i]=TIM_FREQ/(Min_Freq*3)-1;
					}
					
				}
					else
					
				if(step<(accnt*2))  		//脉冲数量不足于做完整加减速
				{
					SPD.full=0;				//没有完整T形加减速
					SPD.run_state=ACCEL;		//加速
					SPD.accel_count=step/2;   	//加速需要步数
					for(i=0;i<SPD.accel_count+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*i)*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						acc[i]=TIM_FREQ/SPD.fre-1;
					}
					SPD.decel_val=(step-SPD.accel_count);	//减速步
					for(i=0;i<SPD.decel_val+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*(SPD.decel_val-i))*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						dec[i]=TIM_FREQ/SPD.fre-1;
					}
				}
				else
					
				if(step>=(accnt*2))
				{
					SPD.full=1;					//有完整T形加减速
					SPD.run_state=ACCEL;		//加速
					SPD.accel_count=accnt;   	//加速需要步数
					for(i=0;i<SPD.accel_count+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*i)*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						acc[i]=TIM_FREQ/SPD.fre-1;
					}
					SPD.decel_start=step-accnt;   	//开始减速步
					SPD.decel_val=accnt;			//减速步
					for(i=0;i<SPD.decel_val+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*(SPD.decel_val-i))*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						dec[i]=TIM_FREQ/SPD.fre-1;
					}
				}
				SPD.done=0;    //清除脉冲发送完成标志置位
				TIM3->ARR=(uint16_t)TIM_FREQ/Min_Freq;
				TIM3->CNT=0;
				HAL_TIM_PWM_Start_IT(&htim3,TIM_CHANNEL_1);    	//打开TIM3 PWM通道1 (正转脉冲CW)
		}
		else						//目标位置小于当前位置,电机反转
		{
			
			SPD.dir=CCW;			//脉冲负计数
			step=SPD.current-Target;  	//计算要移动的脉冲数量
				if(step<20)
				{
					SPD.run_state=DECEL;//直接做减速
					SPD.decel_val=step;		//减速步
					for(i=0;i<SPD.decel_val+1;i++)
					{
						dec[i]=TIM_FREQ/(Min_Freq*3)-1;
					}
				}
					else
					
				if(step<(accnt*2))  //脉冲数量不足于做完整加减速
				{
					SPD.full=0;				//没有完整T形加减速
					SPD.run_state=ACCEL;		//加速
					SPD.accel_count=step/2;   	//加速需要步数
					for(i=0;i<SPD.accel_count+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*i)*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						acc[i]=TIM_FREQ/SPD.fre-1;
					}
					SPD.decel_val=(step-SPD.accel_count);	//减速步
					
					for(i=0;i<SPD.decel_val+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*(SPD.decel_val-i))*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						dec[i]=TIM_FREQ/SPD.fre-1;
					}
				}
				else
					
				if(step>=(accnt*2))
				{
					SPD.full=1;					//有完整T形加减速
					SPD.run_state=ACCEL;		//加速
					SPD.accel_count=accnt;   	//加速需要步数
					for(i=0;i<SPD.accel_count+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*i)*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						acc[i]=TIM_FREQ/SPD.fre-1;
					}
					SPD.decel_start=step-accnt;   	//开始减速步
					SPD.decel_val=accnt;			//减速步
					
					for(i=0;i<SPD.decel_val+1;i++)
					{
						SPD.tsqrt=sqrt(SPD.minfreq*SPD.minfreq/10000+2*SPD.accel/10000*(SPD.decel_val-i))*100.0;
						SPD.fre=(uint32_t)SPD.tsqrt;
						dec[i]=TIM_FREQ/SPD.fre-1;
					}
				}
				SPD.done=0;    //清除脉冲发送完成标志置位
				TIM3->ARR=(uint16_t)TIM_FREQ/Min_Freq;
				TIM3->CNT=0;
				HAL_TIM_PWM_Start_IT(&htim3,TIM_CHANNEL_2);		//打开TIM3 PWM通道2(反转脉冲CCW)
		
		}

	}
	else
	{
		SPD.done=1;    //没有脉冲发送,完成标志置位
	}
}


