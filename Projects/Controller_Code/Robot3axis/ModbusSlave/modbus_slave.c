/**
******************************************************************************
* 文件名程: modbus_slave.c 
* 作    者: 东莞-邓凯哥
* 功    能: Modbus从站接收主站数据并响应
* 硬    件: RD3M-56AC
* 说明：此处程序参考了深圳显控公司触摸屏与51单片机通信范例 
* 软件版本 	1.03
******************************************************************************
**/
#include "modbus_slave.h"
#include "modbuscrc16.h"
#include "regbit.h"

extern UART_HandleTypeDef huart1;

extern uint8_t u1Rxcounter,u1ReceBuf[256],u1SendBuf[256];
uint8_t  *ps=u1SendBuf;
uint8_t  *pr=u1ReceBuf;

uint16_t Rword[100];
int16_t  Dword[2048];            //定义类似PLC  D寄存器的字寻址区，D0---D2047 总共2048个  触摸屏为4x1------4x2000
int16_t  *pd=Dword;
/*****************************************************************************
**函数信息 ：void ModbusSlaveRecirve_Func01()            
**功能描述 ：从站分析01功能码读X,Y,M线圈报文，产生响应报文
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlaveRecirve_Func01()
{
   
	uint16_t StartAddr,BitNum,CrcEnd,SendLength;
	uint16_t i,j;

	StartAddr=(*(pr+2)<<8)+*(pr+3);  	//要返回的起始地址               
	BitNum=(*(pr+4)<<8)+*(pr+5);	   	//要读的字节数量,单位是位
	if((StartAddr+BitNum)<0xb00)    	//小于2000读取的是Mbit位元件
	{
		*(ps+0)=SLAVEADDR;          	//站号
		*(ps+1)=0x01;           		//功能码
		if((BitNum%8)==0)
		*(ps+2)=(BitNum)/8;      		//要返回的字节数
		else
		*(ps+2)=((BitNum)/8)+1;  		//不能整除8的时候要多返回一个字节
		for(i=0;i<*(ps+2);i++)				  
			{
			*(ps+3+i)=0;                //先清零复位
			for(j=0;j<8;j++)            //每8个位状态组成一个字节返回
				{
				*(ps+3+i)=(RD_ppp(StartAddr+i*8+j)<<j)+*(ps+3+i); 
				}	                       
			}
			CrcEnd=Calc_CRC16(ps,(*(ps+2)+3));				//校验
			*(ps+3+*(ps+2))=CrcEnd%256;						//校验低位
			*(ps+4+*(ps+2))=CrcEnd/256;						//校验高位
			SendLength=*(ps+2)+5; 
//			HAL_UART_Transmit_IT(&huart1, ps,SendLength);	//发送响应报文
			HAL_UART_Transmit_DMA(&huart1, ps, SendLength); //发送响应报文
	}
} 
/*****************************************************************************
**函数信息 ：void ModbusSlaveRecirve_Func03()            
**功能描述 ：从站分析03功能码读D寄存器报文，产生响应报文
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlaveRecirve_Func03()
{
	uint16_t StartAddr,BitNum,CrcEnd,SendLength;
	uint16_t i,j;

	StartAddr=(*(pr+2)<<8)+*(pr+3);  	//要返回的起始地址               
	BitNum=(*(pr+4)<<8)+*(pr+5);	   	//要读的字节数量,单位是位
   if((StartAddr+BitNum)>2047)          //
     {
       //errorsend(0x03,0x02);     		//响应寄存器数量超出范围
     }
   else
   {
		*(ps+0)=SLAVEADDR;            		//站号
		*(ps+1)=0x03;           			//功能码
		*(ps+2)=BitNum*2;      				//要返回的字节数是请求报文的第五个字节*2
		for(i=0;i<BitNum;i++)
		{
			*(ps+3+i*2)=((*(pd+StartAddr+i))>>8)&0xff;   	//返回寄存器值的高字节
			*(ps+4+i*2)=(*(pd+StartAddr+i))&0xff;        	//返回寄存器值得低字节
		}
		CrcEnd=Calc_CRC16(ps,(BitNum*2)+3);					//校验
		*(ps+3+BitNum*2)=CrcEnd%256;						//校验低位
		*(ps+4+BitNum*2)=CrcEnd/256;						//校验高位
		SendLength=(BitNum*2)+5; 							//发送数据长度
//		HAL_UART_Transmit_IT(&huart1, ps,SendLength);
		HAL_UART_Transmit_DMA(&huart1, ps, SendLength); 	//发送响应报文
   }
}
/*****************************************************************************
**函数信息 ：void ModbusSlaveRecirve_Func05()            
**功能描述 ：从站分析05功能码写Y,M线圈报文，产生响应报文
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlaveRecirve_Func05()  //05功能码
{

	uint16_t startadd,crc_end;
	uint8_t startaddH,startaddL,sendlength;
	uint8_t bit_valueH,bit_valueL;



	startaddH =*(pr+2);
	startaddL =*(pr+3);
	bit_valueH=*(pr+4);
	bit_valueL=*(pr+5);
	startadd  =(startaddH<<8)+startaddL;  //要写入的地址               
	if(startadd<0xa00)        ////写M,Y线圈       
	{
		if(bit_valueH==0xff&&bit_valueL==0x00)   //置位线圈
			WR_YM(startadd,1) ;
		if(bit_valueH==0x00&&bit_valueL==0x00)   //复位线圈
			WR_YM(startadd,0) ;


		*(ps+0)=SLAVEADDR;            //站号
		*(ps+1)=0x05;           //功能码
		*(ps+2)=startaddH;     //地址高字节
		*(ps+3)=startaddL;     //地址低字节
		*(ps+4)=bit_valueH;     //地址高字节
		*(ps+5)=bit_valueL;     //地址低字节
		crc_end=Calc_CRC16(ps,6);					  //校验
		*(ps+6)=crc_end%256;					  //校验低位
		*(ps+7)=crc_end/256;					  //校验高位		
		sendlength=8; 
//		HAL_UART_Transmit_IT(&huart1,ps,sendlength);
		HAL_UART_Transmit_DMA(&huart1, ps, sendlength); //发送响应报文
	}
} 
/*****************************************************************************
**函数信息 ：void ModbusSlaveRecirve_Func06()            
**功能描述 ：从站分析06功能码写单个D寄存器报文，产生响应报文
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlaveRecirve_Func06()
 {
	 
	  uint16_t startadd,crc_end;
    int16_t wdata_06;
    uint8_t startaddH,startaddL;
    uint8_t wdataH_06,wdataL_06,sendlength;
				
	startaddH=*(pr+2);
	startaddL=*(pr+3);
	wdataH_06=*(pr+4);
	wdataL_06=*(pr+5);
	startadd=(startaddH<<8)+startaddL;  //要写入的起始地址                 
	wdata_06=(wdataH_06<<8)+wdataL_06;	//要写入的数值
	if(startadd<2048)
	{
		*(pd+startadd)=wdata_06;        //将数值写入寄存器
		*(ps+0)=SLAVEADDR;            	//站号
		*(ps+1)=0x06;           		//功能码
		*(ps+2)=startaddH;      		//返回地址高字节
		*(ps+3)=startaddL;      		//返回地址低字节
		*(ps+4)=(uint8_t)(((*(pd+startadd))>>8)&0xff);  //返回寄存器值高字节
		*(ps+5)=(uint8_t)(*(pd+startadd)&0xff);         //返回寄存器值低字节

		crc_end=Calc_CRC16(ps,6);						//校验
		*(ps+6)=crc_end%256;							//校验低位
		*(ps+7)=crc_end/256;							//校验高位
		sendlength=8; 
//		HAL_UART_Transmit_IT(&huart1, ps,sendlength);
		HAL_UART_Transmit_DMA(&huart1, ps, sendlength); //发送响应报文
	}
}
/*****************************************************************************
**函数信息 ：void ModbusSlaveRecirve_Func10()            
**功能描述 ：从站分析10功能码写多个D寄存器报文，产生响应报文
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlaveRecirve_Func10()
{

	uint16_t startadd,crc_end;  
	uint16_t register_num;
	uint8_t startaddH,startaddL;
	uint8_t register_numH,register_numL;
	uint8_t length,sendlength;
	uint16_t i;
	 

	startaddH=*(pr+2);
	startaddL=*(pr+3);
	register_numH=*(pr+4);
	register_numL=*(pr+5);
	startadd=(startaddH<<8)+startaddL;  //要返回的起始地址 
	register_num=(register_numH<<8)+register_numL;   //寄存器数量                
	length=*(pr+6);	                               //要写的字节数量
	if((startadd+(length/2))>2047)                //最多允许写2048个寄存器
	{
		 //errorsend(0x10,0x02);     //响应寄存器数量超出范围
	}
	else
	{
	for(i=0;i<(length/2);i++)        //将值写入寄存器
	{
	 *(pd+startadd+i)=(*(pr+7+i*2)<<8)+(*(pr+8+i*2)&0xff);	
	}
		*(ps+0)=SLAVEADDR;            //站号
		*(ps+1)=0x10;           //功能码
		*(ps+2)=startaddH;      //返回地址高位
		*(ps+3)=startaddL;      //返回地址低位
		*(ps+4)=register_numH;
		*(ps+5)=register_numL;
		crc_end=Calc_CRC16(ps,6);					//校验
		*(ps+6)=crc_end%256;					//校验低位
		*(ps+7)=crc_end/256;					//校验高位
		sendlength=8; 
//		HAL_UART_Transmit_IT(&huart1, ps,sendlength);
		HAL_UART_Transmit_DMA(&huart1, ps, sendlength); //发送响应报文
	}
} 
/*****************************************************************************
**函数信息 ：void ModbusSlave_Command()            
**功能描述 ：从站命令解析
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void ModbusSlave_Command()
{
	uint16_t CrcEnd;
	if(*(pr+0)==SLAVEADDR)
	{
		CrcEnd=Calc_CRC16(pr,u1Rxcounter-2);					  		//校验
		if(CrcEnd==((*(pr+u1Rxcounter-1)<<8 )|*(pr+u1Rxcounter-2)))	  	//当校验一致时
		{
			switch(*(pr+1))
			{
				case 0x01:ModbusSlaveRecirve_Func01(); break; 
				case 0x03:ModbusSlaveRecirve_Func03(); break; 
				case 0x05:ModbusSlaveRecirve_Func05(); break; 
				case 0x06:ModbusSlaveRecirve_Func06(); break; 
				case 0x10:ModbusSlaveRecirve_Func10(); break; 
				default:;break;  
			}
		}
	}
}